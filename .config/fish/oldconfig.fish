pfetch
fortune | cowsay -f tux
alias la="ls -a"
alias rm="rm -i"
alias mv="mv -i"
alias vim="nvim"
alias config="/usr/bin/git --git-dir=$HOME/gitlab-repos/dotsbare/ --work-tree=$HOME"
