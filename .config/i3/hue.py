import os
import sys
from phue import Bridge

try:
    b = Bridge('192.168.1.102')
except OSError:
    print('')
sys.exit(1)
# If the app is not registered and the button is not pressed, press the button and call connect() (this only needs to be run a single time)
#b.connect()
light_names = b.get_light_objects('name')
light = light_names.get('Dining Room Lamp')
if light is None:
    sys.exit(0)
if os.environ.get('BLOCK_BUTTON') == '1':
    light.on = light.on is False
if os.environ.get('BLOCK_BUTTON') == '4':
    light.brightness += 10
elif os.environ.get('BLOCK_BUTTON') == '5':
    light.brightness -= 10
if light.on:
    print('💡{}%'.format(round(100 / 255 * light.brightness)))
else:
    print('💡') 
