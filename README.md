**TuxBunny's Dotfiles**

These are my personal dotfiles, dotfiles are configuration files used by software in Linux and Unix.
They are named this because they have a "." at the beginning (aka a dot). The dot makes the file hidden, and is commonly used with config files. Dotfiles are not always config files, but can be any file that begins with a dot, however the name is usually used for config files.

The Polybar config is from distrotube, but with some modifications by me.
I have noticed on my Pop OS system that the polybar volume and update checker scripts did not work, so these may not work for you, but I have not modified these.

The i3 configuration is default but with mods. It adds media controls, autolaunching EasyEffects, polybar, plus app classes and auto res setting and wallpaper. It uses special fonts (free on github), as well as extra software such as polybar, picom, rofi, playerctl, nitrogen, xrandr, Doom Emacs and is set up to launch alacritty. You can change these if you want. It is also set up with some special keybindings that I use with software such as my browser, Discord and emacs, and DistroTube's dmscripts. These may not work for you if you do not have these.

Support is avaliable via the Issues feature.
I am not responsible for damages caused by this
software.
For minor issues and bugs, please use the Issues feature.
